#!/bin/bash
DOCKERCOMPOSE="docker compose"
res=`which docker-compose`
if [ -n "$res" ]; then
	DOCKERCOMPOSE="docker-compose"
fi

source "$1"

echo "sending reload singal to service 'mbtileserver'"
$DOCKERCOMPOSE -p "$mandant" --env-file "$1" exec mbtileserver kill -HUP 1
