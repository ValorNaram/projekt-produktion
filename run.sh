#!/bin/bash
DOCKERCOMPOSE="docker compose"
res=`which docker-compose`
if [ -n "$res" ]; then
	DOCKERCOMPOSE="docker-compose"
fi

mandantFile=${1//".env"/""}
if [ -z "$mandantFile" ]; then
	# Siehe https://linuxize.com/post/bash-redirect-stderr-stdout/
	echo "Bitte Namen des Mandanten angeben" 1>&2
	echo "USAGE: $0 <name of mandant file in env/>" 1>&2
	exit 1
fi

envPath="$PWD/env"
mandantEnvPath="$envPath/$mandantFile.env"

source "$mandantEnvPath"

services=${services//";"//" "}
services=( $services )

cd services/

oldpwd="$PWD" # speichere den Pfad des aktuellen Arbeitsverzeichnisses

# Iteriere durch alle Services (jeder Service hat einen eigenen Ordner)
#                 Gebe alle Ordner im aktuellen Verzeichnis zurück
#   $folderName ist die Iterationsvariable. Pro Durchlauf enthält sie den Namen eines einzelnen Ordners
for folderName in "${services[@]}"; do
	cd "$folderName" # z.B. 'mbtileserver/'
	$DOCKERCOMPOSE -p "$mandant" --env-file "$mandantEnvPath" up --detach --build
	cd "$oldpwd"
done

